#!/bin/bash

# Create new task
echo "Creating new task:"
curl -X POST -H "Content-Type: application/json" -d '{"task":"New task"}' http://localhost:3000/tasks

# Show all tasks
echo "Showing all tasks:"
curl http://localhost:3000/tasks

# Update a task
echo "Updating task:"
curl -X PUT -H "Content-Type: application/json" -d '{"task":"Updated task"}' http://localhost:3000/tasks/0

# Delete a task
echo "Deleting task:"
curl -X DELETE http://localhost:3000/tasks/0

