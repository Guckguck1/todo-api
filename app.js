const express = require('express');
const app = express();
app.use(express.json());

let tasks = [];

app.get('/tasks', (req, res) => {
    res.json(tasks);
});

app.post('/tasks', (req, res) => {
    tasks.push(req.body);
    res.status(201).send();
});

app.put('/tasks/:id', (req, res) => {
    const id = req.params.id;
    tasks[id] = req.body;
    res.send();
});

app.delete('/tasks/:id', (req, res) => {
    const id = req.params.id;
    tasks.splice(id, 1);
    res.send();
});


app.listen(3000, () => console.log('Server runs on port 3000'));
